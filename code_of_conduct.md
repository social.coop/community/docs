*Suggestions for revision can be made on this [Disroot Pad](https://pad.disroot.org/p/Social.Coop_Code_of_Conduct_V3.1revs)*

--------

# Social.Coop Member Code of Conduct v3.1

## 1. Purpose and Values

(ICA – Cooperative Values and Principles <https://www.ica.coop/en/whats-co-op/co-operative-identity-values-principles>)

As a cooperative, social.coop is based on the values of self-help, self-responsibility, democracy, equality, equity, and solidarity. Our members believe in the ethical values of honesty, openness, social responsibility, and caring for others. We also follow the rest of the ICA co-op principles. 

We welcome anyone able to use our services and willing to accept the responsibilities of membership, regardless of qualities such as gender identity or expression, sexual orientation, disability, mental illness, neuro(a)typicality, physical appearance, body size, age, race, nationality, ethnicity, socioeconomic status, family structure, spirituality, religion (or lack thereof), education, or other personal traits. We particularly celebrate diversity and do not tolerate bigotry or prejudice. Diverse opinions on politics, religion, and other matters are welcome as long as they align with our core values. Let there be no confusion, Social.Coop is anti-racist, anti-fascist, and anti-transphobic. 

## 2. Scope

We expect all social.coop community participants to abide by this Code of Conduct in all community venues and in any other context pertaining to community business, including in-person events.

This code of conduct and its related procedures also applies to unacceptable behaviour by Social.Coop members occurring outside the scope of community activities.

We expect users on other instances to respect our members and treat them according to our Code of Conduct. We will defend our users and protect them from abuse. See the [Federation Abuse Policy](https://git.coop/social.coop/community/docs/wikis/Federation-Abuse-Policy-v1)

## 3. Encouraged Behaviour

I will:    
*  Help maintain a culture of inclusivity and open participation
*  Show care, consideration, and respect for others.
*  Keep in mind the potential for misunderstandings, avoiding initial assumptions of bad faith.
*  Keep criticism constructive.
*  Attempt to de-escalate conflicts as they arise (see [Conflict Resolution Guidelines](https://git.coop/social.coop/community/docs/wikis/Conflict-Resolution-Guidelines-v3.1) ) 
*  Recognize and respect the legitimacy of resistance to abuse and oppression.
*  Participate in an authentic way.


## 4. Unacceptable Behaviour

I will not:
*  Use violence, threats of violence, or violent language directed against another person.
*  Harass  — defined as continuing to interact with or post about another person after having been asked to stop. This includes, but is not limited to, unwelcome sexual attention, deliberate intimidation, stalking, and dogpiling.
*  Make offensive, harmful, or abusive comments or insults, particularly in relation to diverse traits (as referenced in our values above).
*  Advocate or encourage any of the above behaviour.


## 5. Content

I am responsible for the content I share, and will:
- Always use content warnings (CWs) when sharing content that:
  - is likely to be distressing or hurtful to others
  - should require consent to view
  - includes discussions or depictions of violence, sexually explicit material, and/or common PT triggers
  - includes flashing or rapidly changing images
- Strive to make posts accessible with alt-text descriptions for visual media and transcriptions for audio.
- Give credit and recognize the contributions of others.
- Promote products or services provided by individuals, cooperatives, or aligned communities over those provided by non-co-op, for-profit companies.


I will not post, threaten to post, nor share from this instance or external sources:
- Personally Identifiable Information about others. (“doxing”)
- Sexual Material that is either:
  - Without consent, or
  - Depicting individuals appearing to be under age 18 — including Loli.
- Excessively frequent promotions of a product or service (i.e. spam)
- Material that promotes bigotry, including fascist, racist, misogynistic, homophobic, or transphobic content.


## 6. Reporting

I accept the responsibility to report any Code of Conduct violations I see, whether or not they affect me personally.

If I am comfortable and believe it will help, I will consider contacting the violator privately to respectfully ask that they remove the violation.

See [Reporting Guidelines](https://git.coop/social.coop/community/docs/blob/master/reporting_guide.md)

For conflicts or sensitive issues -- i.e, situations that do not involve abuse or other violations of the Code of Conflict -- options include:
1. engaging publicly or privately with others (perhaps with help) to constructively resolve our differences
2. blocking or muting a user
3. reporting the incident to social.coop to seek help in resolving the conflict

See [Conflict Resolution Guidelines](https://git.coop/social.coop/community/docs/wikis/Conflict-Resolution-Guidelines-v3.1)

## 7. Resources

The Community Working Group will maintain a list of resources about codes of conduct and strategies for constructive and healthy discussion online.

## 8. License and Attribution

This Code of Conduct is distributed under a Creative Commons Attribution-ShareAlike 4.0 International license.

Portions of text derived from the Citizen Code of Conduct. Revision 2.2. Posted 6 March 2017.

--------------------------------