# Social.coop Community Documents

This git repository is for developing and maintaining documentation relating to the policy and process of the social.coop Community Working Group.

Please visit [the wiki](https://git.coop/social.coop/community/docs/wikis/home) to find previous versions of our Code of Conduct, Reporting Guidelines, and other information.

Current versions of the Code of Conduct and Reporting Guidelines are also available as markdown (.md) files in the master branch of the repository.

Meeting notes and our agenda are kept in the [CWG CoC Taskforce Living Agenda Etherpad](https://pad.disroot.org/p/CWG_CoC_Taskforce_LivingAgenda).