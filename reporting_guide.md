This is a revised version of the Reporting Guidelines that were approved in a membership vote on 9/1/2018. 

Suggestions for revision can be made on this [Disroot Pad](https://pad.disroot.org/p/Social.Coop_Reporting_Guide_v3.1revs)

---------

**Social.Coop Reporting Guide**

**How to report Code of Conduct violations**

Social.Coop is based on the values of self-help, self-responsibility, democracy, equality, equity, and solidarity. In the tradition of their founders, cooperative members believe in the ethical values of honesty, openness, social responsibility and caring for others.

Our top priority is the safety of each of our members and our community as a whole. We will not tolerate bigotry, including fascist, racist, misogynistic, homophobic, or transphobic content and will act swiftly and decisively to protect our members from abuse.

We would rather get "false alarms" than have Code of Conduct violations go unaddressed. 

If you think someone is violating the Code of Conduct, please report them to the social.coop moderators.

<screenshot of Mastodon's reporting interface>

Click "report @[user]".

<screenshot of sample DM>

Or DM someone on the Community Moderation Team: <list of handles> You can include screenshots, links, and any other context that will help us understand the situation.

**What happens after I make a report?**

You will get an automated email acknowledging that we've received it. We will do our best to get back to you within 24 hours (understanding that people live in different time zones, have different schedules, etc.).

Our first priority is making sure everyone is safe. If necessary, we will:

    Delete, edit, or hide a problem post

    This may be proactive to deal with the item in order to not wait for the person to take care of it

    or this may be necessary when the person will not voluntarily address the item

    Privately tell the violator(s) to knock it off, delete/redact posts on Mastodon/Loomio/in Matrix chat, or apologize

    Publicly tell the violator(s) to knock it off, delete/redact, or apologize

    Mute or block the violator(s)


We may delay an official response until we have dealt with the immediate situation.

Once we have an understanding of what's going on, we will make a decision and contact you. We will take your feedback into account, but the action we take is our responsibility. This might involve:
    

    Nothing (if we determine no violation occurred)

    A temporary ban

    A permanent ban

    A request for a public or private apology

    A request to participate in mediation

We embrace the principles of restorative justice. We want to give people a chance to correct mistakes and learn from them.
If you disagree with our final decision, you can appeal it here: < link to CMT >